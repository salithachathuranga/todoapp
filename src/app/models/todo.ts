export class Todo {
    id: number;
    description: string;
    isCompleted: boolean;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
