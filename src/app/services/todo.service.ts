import { Injectable } from '@angular/core';
import {Observable} from "rxjs";

import {HttpClient} from "@angular/common/http";
import {Todo} from "../models/todo";


@Injectable()
export class TodoService {

  private url = '/assets/data/todos.json'

  constructor(private http: HttpClient) {}

  getTodoList(): Observable<Todo[]>{
    return this.http.get<Todo[]>(this.url);
  }

}
