import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Todo} from "../../models/todo";

@Component({
    selector: 'item',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

    @Input() todo: Todo;
    @Output() deleteTodo: EventEmitter<Todo> = new EventEmitter<Todo>();

    constructor() {
    }

    ngOnInit() {
    }

    setClasses() {
        return {
            'is__complete': this.todo.isCompleted
        };
    }

    completeTodo(todo) {
        todo.isCompleted = !todo.isCompleted;
    }

    removeTodo(todo) {
        this.deleteTodo.emit(todo);
    }

}
