import {Component, OnInit, Input} from '@angular/core';
import {TodoService} from "../services/todo.service";
import {Todo} from "../models/todo";

@Component({
    selector: 'todolist',
    templateUrl: './todolist.component.html',
    styleUrls: ['./todolist.component.css']
})
export class TodolistComponent implements OnInit {

    @Input() appTitle: string;

    private todoItems: Todo[];
    private todoInput: string;

    constructor(private todoService: TodoService) {
    }

    ngOnInit() {
        this.getToDoItemsList();
    }

    getToDoItemsList() {
        this.todoService.getTodoList().subscribe(
            (data: any) => {
                this.todoItems = data.results;
            }
        );
    }

    addTodo(todoForm) {
        let length = this.todoItems.length;
        let todo = new Todo(
            {
                "id": length + 1,
                "description": todoForm.value.todoInput,
                "isCompleted": false
            }
        );
        if (todoForm.valid) {
            this.todoItems.push(todo);
        }
        todoForm.reset();
    }

    removeTodo(todo: Todo) {
        this.todoItems = this.todoItems.filter(todoItem => todoItem.id !== todo.id);
    }

}
